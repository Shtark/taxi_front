// -------   Mail Send ajax

$(document).ready(function () {
    var orderForm = $('#order-taxi'); // contact orderForm
    var orderSubmit = $('.submit-btn'); // orderSubmit button
    var alert = $('.alert-msg'); // alert div for show alert message

    // orderForm orderSubmit event
    orderForm.on('submit', function (e) {
        e.preventDefault(); // prevent default orderForm submit
        $.ajax({
            url: 'order.php', // orderForm action url
            type: 'POST', // orderForm submit method get/post
            dataType: 'html', // request type html/json/xml
            data: orderForm.serialize(), // serialize orderForm data
            beforeSend: function () {
                alert.fadeOut();
                orderSubmit.html('Отправляется....'); // change submit button text
            },
            success: function (data) {
                if (data === 'undifined' && data.status !== 'success') {
                    window.alert(data.status);
                }
                orderForm.trigger('reset'); // reset orderForm
                orderSubmit.html('Заказать'); // change submit button text
                //submit.attr("style", "display: none !important"); // reset submit button text
            },
            error: function (e) {
                console.log(e)
            }
        });
    });
});